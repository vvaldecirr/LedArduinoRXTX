package control;

import java.util.Scanner;

import model.Arduino;

public class LedArduinoTest {

	public static void main(String[] args) {

		Scanner entrada = new Scanner(System.in);
		Arduino a = new Arduino();

		System.out.println("Choose 'on' or 'off'");
		String opcao = entrada.nextLine();
		System.out.println("Entrada: " + opcao);

		while (opcao.equals("on") || opcao.equals("off")) {

			a.comunicacaoArduino(opcao);

			System.out.println("Choose 'on' or 'off'");
			opcao = entrada.nextLine();
			System.out.println("Entrada: " + opcao);
		}
		
		a.comunicacaoArduino(opcao);
		entrada.close();

	}

}
