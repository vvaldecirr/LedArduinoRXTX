package model;

import control.ControlePorta;

/**
 * @author klauder
 */
public class Arduino {
	private ControlePorta cport;

	/**
	 * Construtor da classe Arduino
	 */
	public Arduino() {
		// this.cport = new ControlePorta("COM3",9600);//Windows - porta e taxa de
		// transmissão
		// this.cport = new ControlePorta("/dev/ttyUSB0",9600);//Linux - porta e taxa de
		// transmissão
		this.cport = new ControlePorta("/dev/ttyACM0", 9600);// Linux - oracle vitualbox
	}

	/**
	 * Envia o comando para a porta serial
	 * 
	 * @param button - Botão que é clicado na interface Java
	 */
	public void comunicacaoArduino(String opcao) {
		if ("on".equals(opcao)) {
			this.cport.enviaDados(1);
			System.out.println("Led ACESO");// Imprime o nome do botão pressionado
		} else if ("off".equals(opcao)) {
			this.cport.enviaDados(2);
			System.out.println("Led APAGADO");
		} else {
			this.cport.close();
			System.out.println("Comunicação com porta serial encerrada");// Imprime o nome do botão pressionado
		}
	}
}
